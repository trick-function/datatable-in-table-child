import {useEffect, useState} from "react";
import {BASE_URL} from "../REACTJS/ReactJsHookConetextApi/src/config/path";
import ReactDOM from "react-dom";
import React from "react";
import {LoadingChild} from "../REACTJS/ReactJsHookConetextApi/src/shared/components/LoadingChild/LoadingChild";

const ArrData = (props) => {

    // function toggle show/hide row child
    $('#FareScanning tbody').on('click', 'td.details-control', function () {
        const tr = $(this).closest('tr');
        const row = table.row(tr);
        console.log(row)
        console.log(row.child.isShown())
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');

        }
    });

    const showFileDetails = (files: any) => {
        console.log(files)
        let str = "";
        files.map((val, index) => {
            var carrier = (val.carrier != null) ? val.carrier : ''
            str += '<tr key=' + index + '>' +
                '<td class="text-center">' + (index + 1) + '</td>' +
                '<td class="text-center">' + carrier + '</td>' +
                '<td class="text-center">' + val.arrivalTime + '</td>' +
                '<td class="text-center">' + val.originCode + '</td>' +
                '<td class="text-center">' + val.destinationCode + '</td>' +
                '<td class="text-center">' + val.departureDate + ' ' + val.departureTime + '</td>' +
                '<td class="text-center">' + val.flightNumber + '</td>' +
                '</tr>'
        })
        return str;
    }

    const format = (d: any) => {
        // `d` is the original data object for the row
        return '<div class="my-2 float-right p-0" style="width: 97.6%">' +
            '<table class="table-bordered " style="border-bottom: none !important" cellpadding="5" cellspacing="0" border="0" >' +
            '<thead>' +
            '<tr>' +
            '<th class="text-center" style="width: 5%">STT</th>' +
            '<th class="text-center">Carrier</th>' +
            '<th class="text-center">arrivalTime</th>' +
            '<th  style="width: 30%">Origin</th>' +
            '<th class="text-center" style="width: 20%">Destination</th>' +
            '<th class="text-center" style="width: 20%">Departure Date</th>' +
            '<th class="text-center" style="width: 25%">Flight Number</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody>' +
            `${showFileDetails(d.infoFlight)}` +
            '</tbody>'
        '</table>' +
        '</div>';
    }

    const getData = (arrFilter: any) => {
        setHidden(true)
        const that = this;
        const token = localStorage.getItem('jwtToken');
        table = $("#FareScanning").DataTable({
            "paging": true,
            // "stateSave": true,
            "destroy": true,
            // "order": [[0, "desc"]],
            processing: true,
            serverSide: true,
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "pageLength": 10,
            "searching": false,
            ajax: {
                url: BASE_URL + `/fare/getfare`,
                type: 'POST',
                data: function (d) {
                    // custome data post
                    d.origin = (arrFilter.origin) ? arrFilter.origin : '-1';
                    d.destination = (arrFilter.destination) ? arrFilter.destination : '-1';
                    d.datefrom = (arrFilter.datefrom) ? that.formatYYYYMMDD(arrFilter.datefrom) : '-1';
                   // ...
                },
                "headers":
                    {
                        'Authorization': 'Bearer ' + token,
                        'Accept-Language': 'vi',
                    },
                complete: function () {
                    setHidden(false)
                },
                error: function (xhr) {
                    console.log(xhr.status == 401);
                    {
                        console.clear();
                        // swalError('End of login session')
                        location.href = "/"
                    }
                    // parse "reason" here and take appropriate action
                }
            },
            columns: [
                {data: null, name: null},
                {data: 'pointofview', name: 'pointofview'},
                {data: 'departure', name: 'departure'},
                {data: 'arrival', name: 'arrival'},
                {data: 'carrier', name: 'carrier'},
                {data: 'flightNumber', name: 'flightNumber'},
                {data: 'departureDate', name: 'departureDate'},
                {data: 'departureTime', name: 'departureTime'},
                {data: 'totalPrice', name: 'totalPrice'},
                {data: 'adminFare', name: 'adminFare'},
                {data: 'baseFare', name: 'baseFare'},
            ],
            columnDefs: [ // Custom cell in interface
                {
                    "targets": 0,
                    'orderable': false,
                    'defaultContent': '',
                    "data": "data",
                    "createdCell": (td, cellData, rowData, row, col) => {
                        if (rowData.infoFlight.length > 1) {
                            $(td).addClass('details-control')
                        }
                    }
                },
                {
                    "targets": 1,
                    "data": "data",
                    "createdCell": (td, cellData, rowData, row, col) => {
                        ReactDOM.render( // use for Ract
                            <span>{convertTimestampToDefautFormat(rowData.pointofview)}</span>
                            , td
                        )
                    }
                }
            ],

        })

    }


    return (
        // render html HTML DOM
            <div className={`px-4 mb-4 pt-2 ${(hidden) ? 'd-none' : ''}`}>
                <div className="table-responsive">
                    <table style={{"width": "100%"}} id="FareScanning" className="table table-bordered">
                        <thead>
                        <tr>
                            <th style={{'width': '3%'}}></th>
                            <th>Point of View</th>
                            <th>Departure</th>
                            <th>Arrival</th>
                            <th>Carrier</th>
                            <th>Flight Number</th>
                            <th>Departure Date</th>
                            <th>Departure Time</th>
                            <th>Total Price</th>
                            <th>Admin Fare</th>
                            <th>Base Fare</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
    )

}
